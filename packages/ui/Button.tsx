import React, { CSSProperties } from "react";

export interface ButtonProps {
  backgroundColor?: CSSProperties["backgroundColor"];
}

export function Button({
  children,
  backgroundColor,
}: React.PropsWithChildren<ButtonProps>) {
  return <button style={{ backgroundColor }}>{children || "Empty"}</button>;
}
