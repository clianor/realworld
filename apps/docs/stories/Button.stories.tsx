/* eslint-disable import/no-anonymous-default-export */
import React from "react";
import { Story } from "@storybook/react";

import { Button, ButtonProps } from "ui";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "Example/Button",
  component: Button,
  argTypes: {
    backgroundColor: { control: "color" },
  },
};

const Template = (args: ButtonProps) => <Button {...args} />;

export const Default: Story<ButtonProps> = Template.bind({});
Default.args = {};
